﻿namespace RPG.Core
{
    // list of methods or properties that are implemented in other classes
    // the other classes HAS to include the following methods in their class
    public interface IAction
    {
        void Cancel();
    }
}