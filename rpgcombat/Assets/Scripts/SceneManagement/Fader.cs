﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG.SceneManagement
{

    public class Fader : MonoBehaviour
    {
        CanvasGroup canvasGroup;

        public void FadeOutImmediate()
        {
            canvasGroup.alpha = 1;
        }
        public IEnumerator FadeOut(float time)
        {
            while(canvasGroup.alpha < 1)
            {
                canvasGroup.alpha += Time.deltaTime / time;

                // should be run again on next opportunity, ie: next frame.
                yield return null;
            }
        }

        public IEnumerator FadeIn(float time)
        {
            while (canvasGroup.alpha > 0)
            {
                canvasGroup.alpha -= Time.deltaTime / time;

                // should be run again on next opportunity, ie: next frame.
                yield return null;
            }
        }

        IEnumerator FadeOutIn()
        {
            yield return FadeOut(3f);
            print("faded out...");
            yield return FadeIn(1f);
            print("faded in...");
        }

        // Start is called before the first frame update
        private void Awake()
        {
            canvasGroup = GetComponent<CanvasGroup>();
            // StartCoroutine(FadeOutIn());            
        }

        // Update is called once per frame
        private void Update()
        {

        }
    }
}